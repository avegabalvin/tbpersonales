#pragma once
using namespace System::Drawing;

class Utilitarios
{
private : 

public : 

	Utilitarios();
	~Utilitarios();


	//Fondo de pantalla 
    void Muestra_Fondo(Graphics^g, Image^img);
	void Muestra_Menu(Graphics ^g,Image ^img);
	void Muestra_Instrucciones(Graphics ^g, Image ^img);
	void Muestra_Fondo_juego(Graphics ^g, Image ^img);
};