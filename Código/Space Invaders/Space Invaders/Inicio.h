#pragma once
#include "Utilitarios.h"
#include "Aplicacion.h"
namespace SpaceInvaders {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;
	/// <summary>
	/// Summary for Inicio
	/// </summary>

	public ref class Inicio : public System::Windows::Forms::Form
	{
	public:
		Utilitarios * objUtil;
		Inicio(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Inicio()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pantalla_inicio;

	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Inicio::typeid));
			this->pantalla_inicio = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pantalla_inicio))->BeginInit();
			this->SuspendLayout();
			// 
			// pantalla_inicio
			// 
			this->pantalla_inicio->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pantalla_inicio.Image")));
			this->pantalla_inicio->Location = System::Drawing::Point(0, 0);
			this->pantalla_inicio->Name = L"pantalla_inicio";
			this->pantalla_inicio->Size = System::Drawing::Size(733, 507);
			this->pantalla_inicio->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pantalla_inicio->TabIndex = 0;
			this->pantalla_inicio->TabStop = false;
			// 
			// Inicio
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(733, 505);
			this->Controls->Add(this->pantalla_inicio);
			this->Name = L"Inicio";
			this->Text = L"Inicio";
			this->Load += gcnew System::EventHandler(this, &Inicio::Inicio_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Inicio::Inicio_KeyDown);
			this->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Inicio::Inicio_KeyPress);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pantalla_inicio))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Inicio_Load(System::Object^  sender, System::EventArgs^  e) {

				 
				 
				 SoundPlayer ^player = gcnew SoundPlayer();
				 player->SoundLocation = "C:\Space Invaders Soundtrack.wav";
				 player->PlayLooping();

	}
	
	private: System::Void Inicio_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {

				 
	}


	private: System::Void Inicio_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {

				 if (e->KeyCode== Keys::Enter)
				 {
					 Aplicacion ^app = gcnew Aplicacion();
					 app->Show();
					 
					 
				 }
	}
};
}
