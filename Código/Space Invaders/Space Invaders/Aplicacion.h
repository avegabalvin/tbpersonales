#pragma once
#include"stdafx.h"
#include "Utilitarios.h"

//#include "thread"
namespace SpaceInvaders {

	using namespace System;
	using namespace std;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;
	//using namespace System::Threading;

	/// <summary>
	/// Summary for Aplicacion
	/// </summary>
	public ref class Aplicacion : public System::Windows::Forms::Form
	{

	public: Utilitarios * objUtil;
			bool muestra_fondo = true;
			bool muestra_juego = false;
			bool muestra_instrucciones = false;
			bool muestra_puntaje = false;
			bool menu_activo = true;



	private: System::Windows::Forms::PictureBox^  pbMenu;
	private: System::Windows::Forms::PictureBox^  pbNave;
	private: System::Windows::Forms::PictureBox^  pb_fondo_juego;
	private: System::Windows::Forms::PictureBox^  pbInstrucciones;
	private: System::Windows::Forms::PictureBox^  pbJugar;
	private: System::Windows::Forms::PictureBox^  pbPuntaje;
	private: System::Windows::Forms::PictureBox^  pb_menu_instrucciones;
	private: System::Windows::Forms::PictureBox^  pb_salir;
	private: System::Windows::Forms::PictureBox^  pbMuestraPuntaje;


	public:
		int muestra_menu = 0;

	public:
		Aplicacion(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			objUtil = new Utilitarios();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Aplicacion()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Aplicacion::typeid));
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pbMenu = (gcnew System::Windows::Forms::PictureBox());
			this->pbNave = (gcnew System::Windows::Forms::PictureBox());
			this->pb_fondo_juego = (gcnew System::Windows::Forms::PictureBox());
			this->pbInstrucciones = (gcnew System::Windows::Forms::PictureBox());
			this->pbJugar = (gcnew System::Windows::Forms::PictureBox());
			this->pbPuntaje = (gcnew System::Windows::Forms::PictureBox());
			this->pb_menu_instrucciones = (gcnew System::Windows::Forms::PictureBox());
			this->pb_salir = (gcnew System::Windows::Forms::PictureBox());
			this->pbMuestraPuntaje = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbMenu))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbNave))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pb_fondo_juego))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbInstrucciones))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbJugar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbPuntaje))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pb_menu_instrucciones))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pb_salir))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbMuestraPuntaje))->BeginInit();
			this->SuspendLayout();
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &Aplicacion::timer1_Tick);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(100, 50);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Visible = false;
			// 
			// pbMenu
			// 
			this->pbMenu->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbMenu.Image")));
			this->pbMenu->Location = System::Drawing::Point(317, 11);
			this->pbMenu->Name = L"pbMenu";
			this->pbMenu->Size = System::Drawing::Size(100, 50);
			this->pbMenu->TabIndex = 1;
			this->pbMenu->TabStop = false;
			this->pbMenu->Visible = false;
			// 
			// pbNave
			// 
			this->pbNave->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbNave.Image")));
			this->pbNave->Location = System::Drawing::Point(25, 103);
			this->pbNave->Name = L"pbNave";
			this->pbNave->Size = System::Drawing::Size(100, 50);
			this->pbNave->TabIndex = 2;
			this->pbNave->TabStop = false;
			this->pbNave->Visible = false;
			// 
			// pb_fondo_juego
			// 
			this->pb_fondo_juego->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pb_fondo_juego.Image")));
			this->pb_fondo_juego->Location = System::Drawing::Point(317, 126);
			this->pb_fondo_juego->Name = L"pb_fondo_juego";
			this->pb_fondo_juego->Size = System::Drawing::Size(100, 50);
			this->pb_fondo_juego->TabIndex = 3;
			this->pb_fondo_juego->TabStop = false;
			this->pb_fondo_juego->Visible = false;
			// 
			// pbInstrucciones
			// 
			this->pbInstrucciones->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbInstrucciones.Image")));
			this->pbInstrucciones->Location = System::Drawing::Point(25, 200);
			this->pbInstrucciones->Name = L"pbInstrucciones";
			this->pbInstrucciones->Size = System::Drawing::Size(100, 50);
			this->pbInstrucciones->TabIndex = 4;
			this->pbInstrucciones->TabStop = false;
			this->pbInstrucciones->Visible = false;
			// 
			// pbJugar
			// 
			this->pbJugar->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbJugar.Image")));
			this->pbJugar->Location = System::Drawing::Point(25, 267);
			this->pbJugar->Name = L"pbJugar";
			this->pbJugar->Size = System::Drawing::Size(47, 26);
			this->pbJugar->TabIndex = 5;
			this->pbJugar->TabStop = false;
			this->pbJugar->Visible = false;
			// 
			// pbPuntaje
			// 
			this->pbPuntaje->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbPuntaje.Image")));
			this->pbPuntaje->Location = System::Drawing::Point(25, 299);
			this->pbPuntaje->Name = L"pbPuntaje";
			this->pbPuntaje->Size = System::Drawing::Size(47, 33);
			this->pbPuntaje->TabIndex = 6;
			this->pbPuntaje->TabStop = false;
			this->pbPuntaje->Visible = false;
			// 
			// pb_menu_instrucciones
			// 
			this->pb_menu_instrucciones->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pb_menu_instrucciones.Image")));
			this->pb_menu_instrucciones->Location = System::Drawing::Point(25, 338);
			this->pb_menu_instrucciones->Name = L"pb_menu_instrucciones";
			this->pb_menu_instrucciones->Size = System::Drawing::Size(47, 29);
			this->pb_menu_instrucciones->TabIndex = 7;
			this->pb_menu_instrucciones->TabStop = false;
			// 
			// pb_salir
			// 
			this->pb_salir->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pb_salir.Image")));
			this->pb_salir->Location = System::Drawing::Point(25, 373);
			this->pb_salir->Name = L"pb_salir";
			this->pb_salir->Size = System::Drawing::Size(47, 29);
			this->pb_salir->TabIndex = 8;
			this->pb_salir->TabStop = false;
			this->pb_salir->Visible = false;
			// 
			// pbMuestraPuntaje
			// 
			this->pbMuestraPuntaje->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbMuestraPuntaje.Image")));
			this->pbMuestraPuntaje->Location = System::Drawing::Point(317, 200);
			this->pbMuestraPuntaje->Name = L"pbMuestraPuntaje";
			this->pbMuestraPuntaje->Size = System::Drawing::Size(100, 50);
			this->pbMuestraPuntaje->TabIndex = 9;
			this->pbMuestraPuntaje->TabStop = false;
			this->pbMuestraPuntaje->Visible = false;
			// 
			// Aplicacion
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(761, 495);
			this->Controls->Add(this->pbMuestraPuntaje);
			this->Controls->Add(this->pb_salir);
			this->Controls->Add(this->pb_menu_instrucciones);
			this->Controls->Add(this->pbPuntaje);
			this->Controls->Add(this->pbJugar);
			this->Controls->Add(this->pbInstrucciones);
			this->Controls->Add(this->pb_fondo_juego);
			this->Controls->Add(this->pbNave);
			this->Controls->Add(this->pbMenu);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"Aplicacion";
			this->Text = L"Space Invaders";
			this->Load += gcnew System::EventHandler(this, &Aplicacion::Aplicacion_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Aplicacion::Aplicacion_KeyDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbMenu))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbNave))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pb_fondo_juego))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbInstrucciones))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbJugar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbPuntaje))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pb_menu_instrucciones))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pb_salir))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbMuestraPuntaje))->EndInit();
			this->ResumeLayout(false);

		}

		

		
#pragma endregion
		
	private: void musica()
	{
				SoundPlayer ^player = gcnew SoundPlayer();
				player->SoundLocation = "C:\Space Invaders Soundtrack.wav";
				player->Load();
				player->PlaySync();
	}
	private: System::Void Aplicacion_Load(System::Object^  sender, System::EventArgs^  e) {
				 /*
				 Aplicacion^ f = gcnew Aplicacion;
				 Thread^ oThread = gcnew Thread(gcnew ThreadStart(f, &Aplicacion::musica));
				 oThread->Start();
				*/
				 
	}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
				
		// Buffer
		Graphics^g = this->CreateGraphics();

		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeight = (int)g->VisibleClipBounds.Height;

		BufferedGraphicsContext^ espacioBuffer = BufferedGraphicsManager::Current;

		espacioBuffer->MaximumBuffer = System::Drawing::Size(gWidth + 1, gHeight + 1);
		BufferedGraphics ^ buffer = espacioBuffer->Allocate(g, Drawing::Rectangle(0, 0, gWidth, gHeight));

		buffer->Graphics->Clear(Color::SkyBlue);
		/*
		if (muestra_fondo == true)
		{
			objUtil->Muestra_Fondo(buffer->Graphics, this->pictureBox1->Image);
		}*/
		if (menu_activo == true)
		{
			
			if (muestra_menu == 0)
			{
				objUtil->Muestra_Menu(buffer->Graphics, this->pbMenu->Image);
			}
			/////////////////////  MENU JUGAR
			if (muestra_menu == 1)
			{
				objUtil->Muestra_Menu(buffer->Graphics, this->pbJugar->Image);
			}
			/////////////////////  MENU puntajes
			if (muestra_menu == 2)
			{
				objUtil->Muestra_Menu(buffer->Graphics, this->pbPuntaje->Image);
			}
			/////////////////////  MENU intrucciones
			if (muestra_menu == 3)
			{
				objUtil->Muestra_Menu(buffer->Graphics, this->pb_menu_instrucciones->Image);
			}
			/////////////////////  MENU salir
			if (muestra_menu == 4)
			{
				objUtil->Muestra_Menu(buffer->Graphics, this->pb_salir->Image);
			}
		}
		////////////////////////////////////////
		if (muestra_juego == true)
		{
			objUtil->Muestra_Fondo_juego(buffer->Graphics, this->pb_fondo_juego->Image);
		}
		
		if (muestra_instrucciones == true)
		{
			objUtil->Muestra_Instrucciones(buffer->Graphics, this->pbInstrucciones->Image);
		}
		if (muestra_puntaje == true)
		{
			objUtil->Muestra_Instrucciones(buffer->Graphics, this->pbMuestraPuntaje->Image);
		}
		buffer->Render(g);

		delete buffer;
		delete espacioBuffer;
		delete g;
		
	}
	

private: System::Void Aplicacion_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
			 
			 
		
			 if (e->KeyCode == Keys::Enter)
			 {
				 muestra_fondo = false;
				 menu_activo = true;
				 
			 }	
			 if (e->KeyCode == Keys::Up)
			 {
				 if (menu_activo == true)
				 {
					 if (muestra_juego == false && muestra_instrucciones == false && muestra_puntaje == false)
					 {
						 muestra_menu--;
						 if (muestra_menu < 1)
							 muestra_menu = 4;
					 }
				 }
			 }
			 if (e->KeyCode == Keys::Down)
			 {
				 if (menu_activo == true)
				 { 
					 if (muestra_juego == false && muestra_instrucciones == false && muestra_puntaje==false)
					 {


						 muestra_menu++;
						 if (muestra_menu > 4)
							 muestra_menu = 1;
					 }
					
				 }
			 }
			//MENU
			 if (menu_activo == true)
			 {
				 if (muestra_menu == 1)
				 {
					 if (e->KeyCode == Keys::Enter)
					 {
						 muestra_juego = true;
					 }
				 }
				 if (muestra_menu == 2)
				 {
					 if (e->KeyCode == Keys::Enter)
					 {
						 muestra_puntaje = true;
					 }
				 }if (muestra_menu == 3)
				 {
					 if (e->KeyCode == Keys::Enter)
					 {
						 muestra_instrucciones = true;
					 }
				 }if (muestra_menu == 4)
				 {
					 if (e->KeyCode == Keys::Enter)
					 {
						 Application::Exit();
					 }
				 }
			 }
			 // JUGAR
			 if (e->KeyCode == Keys::NumPad1)
			 {
				 muestra_fondo = false;
				 menu_activo = false;
				 muestra_juego = true;
			 }
			 // INSTRUCCIONES
			 if (e->KeyCode == Keys::NumPad2)
			 {
				 muestra_fondo = false;
				 menu_activo = false;
				 muestra_juego = false;
				 muestra_instrucciones = true;
			 }
			 //Salir
			 if (e->KeyCode == Keys::S)
			 {
				 if (muestra_juego == true)
				 {
					 muestra_juego = false;
					 menu_activo = true;
				 }
				 if (muestra_instrucciones == true)
				 {
					 muestra_instrucciones = false;
					 menu_activo = true;
				 }
				 if (muestra_puntaje == true)
				 {
					 muestra_puntaje = false;
					 menu_activo = true;
				 }
			 }
			 //Salir Juego
			/* if (e->KeyCode == Keys::NumPad3)
			 {
				 Application::Exit();
			 }*/
}


};
}
