#pragma once
using namespace System::Drawing;
class Nave
{
private:
	int x, y, dx;
public:
	Nave();
	~Nave();
	void pintar(Graphics ^g,Image ^img);
	void mover(Graphics ^g);
	void setDx(int d_dx);
	void reiniciar(Graphics ^g);
};

